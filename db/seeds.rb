require 'faker'

10.times do
    Beacon.create(
    id: Faker::Number.number(1),
    name: Faker::Name.first_name,
    major: Faker::Number.number(5),
    minor: Faker::Number.number(5),
    latitude: Faker::Address.latitude,
    longitude: Faker::Address.longitude
    )
end

10.times do
    Campaign.create(
    name: Faker::Name.first_name,
    start_time: Faker::Date.backward(14),
    end_time: Faker::Date.forward(14),
    enabled: Faker::Boolean.boolean,
    beacon_id: Faker::Number.number(1)
    )
end