class CreateCampaignTable < ActiveRecord::Migration[5.2]
  def change
    create_table :campaigns do |t|
            t.string    :name
            t.text      :description
            t.datetime  :start_time   , null: false
            t.datetime  :end_time     , null: false
            t.boolean   :enabled      , null: false
            t.integer   :beacon_id
    end
    add_index :campaigns, :beacon_id, unique: true
  end
end
