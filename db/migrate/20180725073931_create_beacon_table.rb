class CreateBeaconTable < ActiveRecord::Migration[5.2]
  def change
    create_table :beacons do |t|
          t.string  :name
          t.integer :major     ,  null: false
          t.integer :minor     ,  null: false
          t.decimal :latitude  , :precision => 10, :scale => 6
          t.decimal :longitude , :precision => 10, :scale => 6

     end
     add_index :beacons, [:major, :minor], unique: true
  end
end
