class CampaignsController < ApplicationController
    def index
        if( params[:major] and params[:minor] )

             beacon = Beacon.where("major= ? and minor = ?" ,params[:major],params[:minor]).first

            if (beacon)

                beacon_id =  beacon.id

               campaign = Campaign.where(beacon_id: beacon_id ).first
                if (campaign)
                     campaign_start_time =  campaign.start_time
                     campaign_end_time =  campaign.end_time
                     campaign_enabled =  campaign.enabled

                    today = Time.now
                    if (campaign_enabled == false)
                       #if a campaign is disabled
                        msg = {:message => "Campaign has ended.!(campaign is disabled)"}
                        render :json => msg, :status => 500


                    elsif ( today > campaign_end_time )
                       #if a campaign has ended
                        msg = {:message => "Campaign has ended.!"}
                        render :json => msg, :status => 500

                    elsif ( campaign_start_time > today)
                       #new scenario, if the campaign will start soon
                         msg = {:message => "Campaign will start soon.!"}
                         render :json => msg, :status => 500

                    else
                       #normal case , during the campaign period and it is enabled
                         render :json => campaign

                    end
                else
                    msg = {:message => "No campaign matches this beacon.!"}
                   render :json => msg, :status => 404
                end
            else
                 msg = {:message => "The beacon dose not exist in the database.!"}
                 render :json => msg, :status => 404
             end
         else
              msg = {:message => "Please, set the major and minor values.!"}
             render :json => msg, :status => 400
         end
     end
end

